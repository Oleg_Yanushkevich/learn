﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Learning
{
    class Program
    {
        static void Main(string[] args)
        {
            Task<int> task = new Task<int>(() => 10 * 10);

            var result = task.Result;
            Console.WriteLine(result);

            // comment for commit
            Console.ReadKey();
            Console.WriteLine("Hello world!");

            int var = 2 + 2;
            Console.WriteLine(var);

            //pa pa pa
            
            //commit from temp branch
            //la la la la
        }
    }
}
